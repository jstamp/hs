{-# LANGUAGE ForeignFunctionInterface #-}

import Foreign.C.String
import Foreign.C.Types
import Foreign.Storable
import Foreign.Marshal.Alloc
import Foreign.Marshal.Array
import Foreign.Ptr
import Text.Printf
import Data.Maybe
import Data.Bits
import Data.Char
import Data.List
import Data.Array
import Data.Word  
import Debug.Trace
import System.Exit
import System.IO.Error
import Control.Exception
import Control.Monad

import OSLib.Wimp
import OSLib.OS
import OSLib.Report
import OSLib.Toolbox
import OSLib.Toolbox.Types
import OSLib.ScrollList
import OSLib.WritableField

--actionOpenParent :: Action
actionOpenParent = 0x100

--actionSaveTags :: Action
actionSaveTags = 0x102

cmpApps :: Cmp
cmpApps = 1

cmpTags :: Cmp
cmpTags = 2

cmpSearch :: Cmp
cmpSearch = 5

choicesFile :: String
choicesFile = "RAM::RamDisc0.$.AppsChoices"

readChoices :: String -> [(String, [String])]
readChoices file = map (\ws ->
  case words ws of
    (app:ts) -> (app, ts)
    _ -> error "Bad format")
  (lines file)

leaf :: String -> String
leaf p = (reverse . fst) $ break (\c -> c == '.') (reverse p)

appname :: String -> Maybe String
appname leaf = case leaf of
    ('!':app) -> Just app
    _ -> Nothing

data App = App
  { path :: String
  , name :: String
  , tags :: [String]
  } deriving Show

process_var :: [(String, [String])] -> String -> Maybe App
process_var info val = do
  app <- appname (leaf val)
  let ts = fromMaybe ([] :: [String]) (lookupTags info app)
  return $ App {path = val, name = app, tags = ts}

lookupTags :: [(String, [String])] -> String -> Maybe [String]
lookupTags info app = do
  (_, ts) <- find (\(a, _) -> a == app) info
  return ts

messageQuit = 0 :: Message
wimpUserMessage = 17 :: Event
toolboxEvent = 512 :: Event
windowDialogueCompleted = 0x82890 :: Action
actionScrollListSelected = 0x140181 :: Action
actionWritableFieldValueChanged = 0x82885 :: Action

main = do
  file <- readFile choicesFile
  xreport_text0 $ show (readChoices file)
  vals <- os_read_var_vals "*$Dir"
  let leaves = catMaybes $ map (process_var (readChoices file)) vals
  let apps = arrayFromList leaves
  let initial_mirror = arrayFromList [0 .. length leaves]
  alloca $ \ptr_tblock -> do
    toolbox_initialise 300 [messageQuit]
      [ actionScrollListSelected
      , windowDialogueCompleted
      , actionOpenParent
      , actionSaveTags
      , actionWritableFieldValueChanged
      ] "<Apps$Dir>" ptr_tblock
    o <- toolbox_create_object "Window"
    toolbox_show_object o
    mapM_ (\App {name = app} ->
      scrolllist_add_item o cmpApps app ('!':app) (-1))
      leaves
    allocaBytes 256 $ \ptr_block ->
      pollLoop ptr_block ptr_tblock apps initial_mirror

data Result = NoMatch | Match deriving (Eq, Show)

lower :: String -> String
lower s = map toLower s

search :: String -> App -> Result
search str (App {path = path, name = name, tags = tags}) = 
  if isPrefixOf lstr lname || or (map (isPrefixOf lstr) ltags)
  then Match
  else NoMatch
  where lstr = lower str
        lname = lower name
        ltags = map lower tags  

search2 :: String -> App -> Bool
search2 str app = search str app == Match

arrayFromList :: [a] -> Maybe (Array Int a)
arrayFromList xs =
  if n > 0
  then Just $ array (0, n - 1) (zip [0 .. n - 1] xs)
  else Nothing
  where n = length xs

addList :: Obj -> Cmp -> Maybe (Array Int App) -> Maybe (Array Int Int) -> IO ()
addList _   _    Nothing     _            = return ()
addList _   _    _           Nothing      = return ()
addList obj cmp (Just apps) (Just mirror) =
  mapM_ (\i ->
    let App {name = app} = apps!i
    in scrolllist_add_item obj cmp app ('!':app) (-1))
  (elems mirror)

pollLoop :: Ptr () -- Wimp block
         -> Ptr Block -- Toolbox block
         -> Maybe (Array Int App) -- Array of all apps
         -> Maybe (Array Int Int) -- Only the apps being shown right now
         -> IO ()
pollLoop ptr_block ptr_tblock mapps mmirror = do
  event <- c_wimp_poll (fromIntegral wimp_MASK_NULL) ptr_block nullPtr
  xreport_text0 $ "Wimp event:" ++ show event
  let continue x y = pollLoop ptr_block ptr_tblock x y
  case event of
    event | event == wimpUserMessage -> do
            action <- peekByteOff ptr_block 16
            xreport_text0 $ printf "User message:&%x" ((fromIntegral action) :: Int)
            case action of
              action | action == messageQuit -> exitSuccess
              _ -> return ()
          | event == toolboxEvent -> do
            action <- peekByteOff ptr_block 8
            xreport_text0 $ printf "Toolbox event:&%x" ((fromIntegral action) :: Int)
            tblock <- peek ptr_tblock
            case action of
              action | action == actionScrollListSelected -> do
                        xreport_text0 "Scroll List selection"
                        flag <- peekByteOff ptr_block 16
                        index <- peekByteOff ptr_block 20
                        text <- scrolllist_get_item_text (this_obj tblock) (this_cmp tblock) index
                        xreport_text0 $ show index
                        let apps = fromMaybe undefined mapps
                        let mirror = fromMaybe undefined mmirror
                        let App {path = path} = apps!(mirror!index)
                        xreport_text0 $ "Item:" ++ show text ++ ", path:" ++ path
                        xreport_text0 $ "Flag:" ++ show flag
                        if (flag :: Word32) .&. 2 /= 0
                          then do os_cli $ "Filer_Run " ++ path
                          else do
                            writablefield_set_value (this_obj tblock) cmpTags ((unwords.tags) (apps!(mirror!index)))
                     | action == actionWritableFieldValueChanged -> do
                        case this_cmp tblock of
                          cmp | cmp == cmpTags -> do
                                  let apps = fromMaybe undefined mapps
                                  let mirror = fromMaybe undefined mmirror
                                  index' <- scrolllist_get_selected (this_obj tblock) cmpApps 0
                                  let index = if index' == -1 then 0 else index'
                                  new <- peekCString $ castPtr (plusPtr ptr_block 16)
                                  xreport_text0 $ show index
                                  continue (Just (apps // [(index, (apps!(mirror!index)) {tags = words new})])) mmirror
                              | cmp == cmpSearch -> do
                                  let apps = fromMaybe undefined mapps
                                  let mirror = fromMaybe undefined mmirror
                                  scrolllist_delete_items (this_obj tblock) cmpApps (-1) (-2)
                                  writablefield_set_value (this_obj tblock) cmpTags ""
                                  --xreport_dump (castPtr (plusPtr ptr_block 16)) 32 4
                                  search_str <- peekCString $ castPtr (plusPtr ptr_block 16)
                                  xreport_text0 $ printf "'%s'" search_str
                                  let new_mirror = (arrayFromList . map fst) $ 
                                                   (filter (\(i,a) -> search2 search_str a)
                                                    (assocs apps))
                                  addList (this_obj tblock) cmpApps mapps new_mirror
                                  continue mapps new_mirror
                     | action == actionOpenParent-> do
                        xreport_text0 "Open parent"
                        index' <- scrolllist_get_selected (parent_obj tblock) cmpApps 0
                        let apps = fromMaybe undefined mapps
                        let mirror = fromMaybe undefined mmirror
                        let index = if index' == -1 then 0 else index'
                        let App {path = path} = apps!(mirror!index)
                        os_cli $ "Filer_OpenDir " ++ path ++ ".^"
                     | action == actionSaveTags -> do
                        let info = map (\App {name = n, tags = ts} -> 
                                     unwords (n:ts)) $
                                     filter (\a->
                                       tags a /= []) 
                                       (case mapps of Nothing -> []; Just apps -> (elems apps))
                        writeFile choicesFile $ unlines info
                     | action == windowDialogueCompleted -> do
                        xreport_text0 "Quit"
                        exitSuccess
              _ -> do
                xreport_text0 "else"
                return ()
    _ -> return ()
  continue mapps mmirror
