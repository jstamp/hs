{-# LANGUAGE ForeignFunctionInterface #-}

module OSLib.OS
(
  ErrNum,
  Error(..),
  c_os_new_line,
  os_write0,
  os_writen,
  os_writec,
  os_readc,
  os_read_monotonic_time,
  my_os_read_line,
  os_read_ram_fs_limits,
  os_read_vdu_variables,
  os_read_mode_variable_current,
  os_read_var_val,
  os_read_var_vals,
  os_cli,
  Box(..)
) where

import Foreign.C.Types
import Foreign.C.String
import Foreign.Marshal.Alloc
import Foreign.Marshal.Array
import Foreign.Ptr
import Foreign.Storable
import Data.Word

foreign import ccall "oslib/os.h os_new_line" c_os_new_line :: IO ()
foreign import ccall "oslib/os.h os_write0" c_os_write0 :: CString -> IO ()
foreign import ccall "oslib/os.h os_writen" c_os_writen :: CString -> Int -> IO ()
foreign import ccall "oslib/os.h os_writec" c_os_writec :: Word8 -> IO ()
foreign import ccall "oslib/os.h os_readc" c_os_readc :: CString -> IO ()
foreign import ccall "oslib/os.h os_read_mode_variable" c_os_read_mode_variable :: CInt -> CInt -> Ptr CInt -> IO ()
foreign import ccall "oslib/os.h os_read_monotonic_time" c_os_read_monotonic_time :: IO CInt
foreign import ccall "oslib/os.h os_read_line32" c_os_read_line32 :: CString -> CInt -> CChar -> CChar -> CInt -> Ptr CInt -> IO ()
foreign import ccall "oslib/os.h os_read_ram_fs_limits" c_os_read_ram_fs_limits :: Ptr CInt -> Ptr CInt -> IO ()
foreign import ccall "oslib/os.h os_read_vdu_variables" c_os_read_vdu_variables :: Ptr CInt -> Ptr CInt -> IO ()
foreign import ccall "oslib/os.h xos_read_var_val" c_xos_read_var_val :: CString -> CString -> CInt -> CInt -> CInt -> Ptr CInt -> Ptr CInt -> Ptr CInt -> IO CInt
foreign import ccall "oslib/os.h os_cli" c_os_cli :: CString -> IO ()

type ErrNum = Word32

data Error = Error
  { errnum :: ErrNum
  , errmess :: String
  } deriving Show

instance Storable Error where
  sizeOf _ = 256
  alignment _ = 4
  peek ptr = do
    n <- peekByteOff ptr 0
    m <- peekCString ((castPtr ptr) `plusPtr` 4)
    return $ Error {errnum = n, errmess = m}

os_cli :: String -> IO ()
os_cli c = withCString c c_os_cli

-- TODO Lookup size first
os_read_var_val :: String -> IO String
os_read_var_val name = do
  ptr_name <- newCString name
  ptr_buffer <- mallocBytes 1024
  ptr_used <- malloc
  e <- c_xos_read_var_val ptr_name ptr_buffer 1024 0 0 ptr_used nullPtr nullPtr
  used <- peek ptr_used
  val <- peekCStringLen (ptr_buffer, fromIntegral used)
  free ptr_name
  free ptr_buffer
  free ptr_used
  return val

read_var_loop ptr_name ptr_buffer ptr_used ptr_context = do
  context <- peek ptr_context
  e <- c_xos_read_var_val ptr_name ptr_buffer 1024 context 0 ptr_used ptr_context nullPtr
  if e == 0
    then do
      used <- peek ptr_used
      val <- peekCStringLen (ptr_buffer, fromIntegral used)
      rest <- read_var_loop ptr_name ptr_buffer ptr_used ptr_context
      return $ val : rest
    else
      return []

-- TODO Lookup size first
-- Returns all matches
os_read_var_vals :: String -> IO [String]
os_read_var_vals name = do
  withCString name $ \ptr_name ->
    allocaBytes 1024 $ \ptr_buffer ->
      alloca $ \ptr_used ->
        alloca $ \ptr_context -> do
          poke ptr_context 0
          vals <- read_var_loop ptr_name ptr_buffer ptr_used ptr_context
          return vals

os_write0 :: String -> IO ()
os_write0 str = withCString str c_os_write0

os_writen :: String -> IO ()
os_writen str = withCStringLen str $ \(cstr, len) -> c_os_writen cstr len

os_writec :: Word8 -> IO ()
os_writec c = c_os_writec c

os_readc :: IO Char
os_readc = alloca $ \ptr_c -> do
  c_os_readc ptr_c
  c <- peek ptr_c
  return $ castCCharToChar c

os_read_mode_variable_current :: Int -> IO Int
os_read_mode_variable_current var = alloca $ \ptr_val -> do
  c_os_read_mode_variable (fromIntegral (-1)) (fromIntegral var) ptr_val
  val <- peek ptr_val
  return $ fromIntegral val

os_read_monotonic_time :: IO Int
os_read_monotonic_time = do
  t <- c_os_read_monotonic_time
  return $ fromIntegral t

my_os_read_line :: Int -> IO String
my_os_read_line bufsz = do
  buffer <- mallocArray bufsz
  ptr_used <- malloc
  c_os_read_line32 buffer (toEnum bufsz) (toEnum 0) (toEnum 255) (toEnum 0) ptr_used
  used <- peek ptr_used
  str <- peekCStringLen (buffer, fromEnum used)
  free buffer
  return str

os_read_ram_fs_limits :: IO (Int, Int)
os_read_ram_fs_limits =
  alloca $ \ptr_start ->
    alloca $ \ptr_end -> do
      c_os_read_ram_fs_limits ptr_start ptr_end
      start <- peek ptr_start
      end <- peek ptr_end
      return (fromIntegral start, fromIntegral end)

os_read_vdu_variables :: [Int] -> IO [Int]
os_read_vdu_variables nums =
  withArray (map toEnum (nums ++ [-1])) $ \ptr_vars ->
    allocaArray (length nums) $ \ptr_vals -> do
      c_os_read_vdu_variables ptr_vars ptr_vals
      vals <- peekArray (length nums) ptr_vals
      return $ map fromIntegral vals

data Box = Box
  { x0 :: Word32
  , y0 :: Word32
  , x1 :: Word32
  , y1 :: Word32
  } deriving (Show)

instance Storable Box where
  sizeOf _ = 16
  alignment _ = 4
  peek ptr = do
    x0 <- peekByteOff ptr 0
    y0 <- peekByteOff ptr 4
    x1 <- peekByteOff ptr 8
    y1 <- peekByteOff ptr 12
    return $ Box {x0 = x0, y0 = y0, x1 = x1, y1 = y1}
