module OSLib.URL
(
  url_register,
  url_deregister,
  url_get_url,
  url_read_data,
  xurl_get_url,
  xurl_read_data,
  Session,
  FetchStatus,
  Method,
  methodGet
) where

import Foreign.C.String
import Foreign.C.Types
import Foreign.Storable
import Foreign.Marshal.Alloc
import Foreign.Ptr
import Data.Word

import OSLib.OS
import OSLib.Report

type Session = Word32
type FetchStatus = Word32
type GetURLFlags = Word32
type Method = Word32

methodGet :: Method
methodGet = 1

foreign import ccall "oslib/url.h url_register" c_url_register :: Word32 -> IO Session
foreign import ccall "oslib/url.h url_deregister" c_url_deregister :: Word32 -> Session -> IO FetchStatus
foreign import ccall "oslib/url.h url_get_url" c_url_get_url :: GetURLFlags -> Session -> Method -> CString -> CInt -> CInt -> CInt -> IO FetchStatus
foreign import ccall "oslib/url.h xurl_get_url" c_xurl_get_url :: GetURLFlags -> Session -> Method -> CString -> CInt -> CInt -> CInt -> Ptr FetchStatus -> IO (Ptr Error)
foreign import ccall "oslib/url.h url_read_data" c_url_read_data :: Word32 -> Session -> Ptr () -> CInt -> Ptr FetchStatus -> Ptr CInt -> IO CInt
foreign import ccall "oslib/url.h xurl_read_data" c_xurl_read_data :: Word32 -> Session -> Ptr () -> CInt -> Ptr FetchStatus -> Ptr CInt -> Ptr CInt -> IO (Ptr Error)

url_register :: Word32 -> IO Session
url_register = c_url_register

url_deregister :: Word32 -> Session -> IO FetchStatus
url_deregister = c_url_deregister

url_get_url :: GetURLFlags -> Session -> Method -> String -> Ptr () -> Int -> String -> IO FetchStatus
url_get_url f s m url b bsz agent =
  withCString url $ \ptr_url ->
    withCString agent $ \ptr_agent ->
      c_url_get_url f s m ptr_url 0 (fromIntegral bsz) 0

xurl_get_url :: GetURLFlags -> Session -> Method -> String -> Ptr () -> Int -> String -> IO (Either FetchStatus Error)
xurl_get_url f s m url b bsz agent =
  withCString url $ \ptr_url ->
    withCString agent $ \ptr_agent ->
      alloca $ \ptr_fetchstatus -> do
        ptr_e <- c_xurl_get_url f s m ptr_url 0 (fromIntegral bsz) 0 ptr_fetchstatus
        xreport_text0 $ show ptr_e
        if show ptr_e /= "0"
          then do
            e <- peek ptr_e
            return $ Right e
          else do
            fetchstatus <- peek ptr_fetchstatus
            return $ Left fetchstatus

url_read_data :: Word32 -> Session -> Ptr () -> Int -> IO (FetchStatus, Int, Int)
url_read_data f s b bsz =
  alloca $ \ptr_status ->
    alloca $ \ptr_left -> do
      poke ptr_status 0
      poke ptr_left 0
      poke (castPtr b) (0 :: Word32)
      read <- c_url_read_data f s b (fromIntegral bsz) ptr_status ptr_left
      status <- peek ptr_status
      left <- peek ptr_left
      return $ (status, fromIntegral left, fromIntegral read)

xurl_read_data :: Word32 -> Session -> Ptr () -> Int -> IO (Either (FetchStatus, Int, Int) Error)
xurl_read_data f s b bsz =
  alloca $ \ptr_status ->
    alloca $ \ptr_left -> do
      poke ptr_status 0
      poke ptr_left 0
      poke (castPtr b) (0 :: Word32)
      alloca $ \ptr_read -> do
        ptr_e <- c_xurl_read_data f s b (fromIntegral bsz) ptr_status ptr_read ptr_left
        if show ptr_e /= "0"
          then do
            e <- peek ptr_e
            return $ Right e
          else do
            status <- peek ptr_status
            left <- peek ptr_left
            read <- peek ptr_read
            return $ Left (status, fromIntegral left, fromIntegral read)
