{-# LANGUAGE ForeignFunctionInterface #-}

module OSLib.Report
(
  xreport_text0,
  report_text0,
  xreport_dump
) where

import Foreign.C.String
import Foreign.C.Types
import Foreign.Ptr

foreign import ccall "oslib/report.h xreport_text0" c_xreport_text0 :: CString -> IO ()
foreign import ccall "oslib/report.h xreport_dump" c_xreport_dump :: Ptr () -> CInt -> CInt -> IO ()

xreport_text0 = report_text0

report_text0 :: String -> IO ()
report_text0 str = do
  withCString str c_xreport_text0

xreport_dump :: Ptr () -> Int -> Int -> IO ()
xreport_dump address l w = do
  c_xreport_dump address (fromIntegral l) (fromIntegral w)
