{-# LANGUAGE ForeignFunctionInterface #-}

module OSLib.OSArgs
(
  my_osargs_read_path
) where

import Foreign.C.Types
import Foreign.C.String
import Foreign.Marshal.Array

foreign import ccall "oslib/osargs.h osargs_read_path" c_osargs_read_path :: CInt -> CString -> CInt -> IO ()

my_osargs_read_path :: Int -> IO String
my_osargs_read_path handle = do
  let buffsz = 1024
  ptr_buffer <- mallocArray buffsz
  c_osargs_read_path (toEnum handle) ptr_buffer (toEnum buffsz)
  peekCString ptr_buffer
