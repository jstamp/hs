module OSLib.Toolbox.Types
(
  Obj,
  Cmp,
  Block(..),
  Task,
  
  Message,
  Action,
  
  InitialiseFlags,
  CreateFlags,
  ShowFlags,
  
  MessageList,
  ActionList,
  MessageTransControlBlock
) where

import Foreign.Storable
import Foreign.Ptr
import Data.Word

type InitialiseFlags = Word32
type CreateFlags = Word32
type ShowFlags = Word32

type MessageList = Ptr Word32
type ActionList = Ptr Word32
type MessageTransControlBlock = Ptr ()
type Task = Word32
type Obj = Word32
type Cmp = Word32

-- Not in OSLib, I put this in
type Message = Word32
type Action = Word32

data Block = Block
  { ancestor_obj :: Obj
  , ancestor_cmp :: Cmp
  , parent_obj :: Obj
  , parent_cmp :: Cmp
  , this_obj :: Obj
  , this_cmp :: Cmp
  } deriving Show

instance Storable Block where
  sizeOf _ = 6 * sizeOf (0 :: Word32)
  alignment _ = 4
  peek ptr = do
    ao <- peekByteOff ptr 0
    ac <- peekByteOff ptr 4
    po <- peekByteOff ptr 8
    pc <- peekByteOff ptr 12
    to <- peekByteOff ptr 16
    tc <- peekByteOff ptr 20
    return $ Block {ancestor_obj = ao, ancestor_cmp = ac, parent_obj = po, parent_cmp = pc, this_obj = to, this_cmp = tc}
