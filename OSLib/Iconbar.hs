module OSLib.Iconbar
(
  iconbar_set_sprite
) where

import Foreign.C.String
import Data.Word
import OSLib.Toolbox.Types

foreign import ccall "oslib/iconbar.h iconbar_set_sprite" c_iconbar_set_sprite :: Word32 -> Obj -> CString -> IO ()

iconbar_set_sprite :: Obj -> String -> IO ()
iconbar_set_sprite obj str = withCString str (c_iconbar_set_sprite 0 obj)
