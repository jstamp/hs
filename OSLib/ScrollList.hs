module OSLib.ScrollList
(
  scrolllist_add_item,
  scrolllist_delete_items,
  scrolllist_get_item_text,
  scrolllist_get_selected
) where

import Foreign.C.Types
import Foreign.C.String
import Foreign.Ptr
import Foreign.Marshal.Alloc
import Foreign.Marshal.Array
import Foreign.Storable

import Data.Word
import OSLib.Toolbox.Types

type AddItemFlags = Word32

foreign import ccall "oslib/scrolllist.h scrolllist_add_item" c_scrolllist_add_item :: AddItemFlags -> Obj -> Cmp -> CString -> Ptr () -> CString -> CInt -> IO ()
foreign import ccall "oslib/scrolllist.h scrolllist_delete_items" c_scrolllist_delete_items :: Word32 -> Obj -> Cmp -> CInt -> CInt -> IO ()
foreign import ccall "oslib/scrolllist.h scrolllist_get_item_text" c_scrolllist_get_item_text :: Word32 -> Obj -> Cmp -> CString -> CInt -> CInt -> IO CInt
foreign import ccall "oslib/scrolllist.h scrolllist_get_selected" c_scrolllist_get_selected :: Word32 -> Obj -> Cmp -> CInt -> IO CInt

scrolllist_add_item :: Obj -> Cmp -> String -> String -> Int -> IO ()
scrolllist_add_item obj cmp text sprite index =
  withCString text $ \ptr_text ->
    withCString sprite $ \ptr_sprite ->
      c_scrolllist_add_item 0 obj cmp ptr_text nullPtr ptr_sprite (fromIntegral index)

scrolllist_delete_items :: Obj -> Cmp -> Int -> Int -> IO ()
scrolllist_delete_items obj cmp start end =
  c_scrolllist_delete_items 0 obj cmp (fromIntegral start) (fromIntegral end)

-- TODO check size?
scrolllist_get_item_text :: Obj -> Cmp -> Int -> IO String
scrolllist_get_item_text obj cmp index =
  allocaBytes 1024 $ \ptr_text -> do
    c_scrolllist_get_item_text 0 obj cmp ptr_text 1024 (fromIntegral index)
    peekCString ptr_text

scrolllist_get_selected :: Obj -> Cmp -> Int -> IO Int
scrolllist_get_selected obj cmp offset = do
  index <- c_scrolllist_get_selected 0 obj cmp (fromIntegral offset)
  return (fromIntegral index)
