{-# LANGUAGE ForeignFunctionInterface #-}

module OSLib.Wimp
(
  wimp_initialise,
  wimp_closedown,
  wimp_poll,
  c_wimp_poll_idle,
  c_wimp_poll,
  wimp_USER_MESSAGE,
  wimp_MASK_NULL,
  Event,
  Window,
  Draw(..)
) where

import Foreign.C.Types
import Foreign.C.String
import Foreign.Marshal.Alloc
import Foreign.Marshal.Array
import Foreign.Ptr
import Foreign.Storable
import Data.Word
import Data.Array
import Data.Bits

import OSLib.ScrollList
import OSLib.Report
import OSLib.OS
import OSLib.Toolbox
import OSLib.Toolbox.Types

import Text.Printf

wimp_USER_MESSAGE :: Int
wimp_USER_MESSAGE = 17

wimp_MASK_NULL :: Word32
wimp_MASK_NULL = 1

type Event = Word32
type Time = Int

foreign import ccall "oslib/wimp.h wimp_initialise" c_wimp_initialise :: CInt -> CString -> Ptr CInt -> Ptr CInt -> IO CInt
foreign import ccall "oslib/wimp.h wimp_poll" c_wimp_poll :: Word32 -> Ptr () -> Ptr CInt -> IO Event
foreign import ccall "oslib/wimp.h wimp_poll_idle" c_wimp_poll_idle :: Word32 -> Ptr () -> CInt -> Ptr CInt -> IO CInt

-- N.B. Caller must make sure list ends in quit
wimp_initialise :: Int -> String -> [Int] -> IO (Int, Int)
wimp_initialise version name message_list =
  withArray (map fromIntegral message_list) $ \ptr_msgs ->
    alloca $ \ptr_version_out ->
      withCString name $ \ptr_name -> do
        handle <- c_wimp_initialise (toEnum version) ptr_name ptr_msgs ptr_version_out
        version_out <- peek ptr_version_out
        return (fromEnum version_out, fromEnum handle)

wimp_poll :: Int -> IO Int
wimp_poll mask = do
  ptr_block <- mallocBytes 256
  event <- c_wimp_poll (toEnum mask) ptr_block nullPtr
  free ptr_block
  return (fromEnum event)

wimp_closedown = undefined

newtype Window = Window (Ptr ())

instance Show Window where
  show _ = "<Window>"

data Draw = Draw
  { w :: Window,
    box :: Box,
    xscroll :: Int,
    yscroll :: Int,
    clip :: Box
  } deriving Show

instance Storable Draw where
  sizeOf _ = 20
  alignment _ = 4
  peek ptr = do
   -- w <- peekByteOff ptr 0
    box <- peekByteOff ptr 4
    xscroll <- peekByteOff ptr 8
    yscroll <- peekByteOff ptr 12
    clip <- peekByteOff ptr 16
--    return $ Window {w = undefined, box = _, xscroll = _, yscroll = _, clip = _}
    return $ Draw {w = undefined, box = box, xscroll = xscroll, yscroll = yscroll, clip = clip}
