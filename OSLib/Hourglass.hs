{-# LANGUAGE ForeignFunctionInterface #-}

module OSLib.Hourglass
(
  hourglass_on,
  hourglass_off,
  hourglass_percentage
) where

import Foreign.C.Types

foreign import ccall "oslib/hourglass.h hourglass_on" c_hourglass_on :: IO()
foreign import ccall "oslib/hourglass.h hourglass_off" c_hourglass_off :: IO()
foreign import ccall "oslib/hourglass.h hourglass_percentage" c_hourglass_percentage :: CInt -> IO()

hourglass_on :: IO ()
hourglass_on = c_hourglass_on

hourglass_off :: IO ()
hourglass_off = c_hourglass_off

hourglass_percentage :: Int -> IO ()
hourglass_percentage p = c_hourglass_percentage (fromIntegral p)
